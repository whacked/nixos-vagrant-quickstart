{ config, pkgs, ... }: with pkgs;

let hs = pkgs.haskellPackages;
    # define our local set of packages to include with hoogle
    hooglePackages = [
      hs.basePrelude
      hs.snapCore
      hs.heist
    ];
in
{

  environment.systemPackages = [
    ghc.ghc783
    haskellPackages.hoogleLocal
  ];

  # override the hoogleLocal package definition to use a custom package set
  nixpkgs.config = {
    packageOverrides = pkgs: rec {

      haskellPackages =
        let callPackage = pkgs.lib.callPackageWith haskellPackages;
        in pkgs.recurseIntoAttrs (pkgs.haskellPackages.override {
          extension = self: super: {
            hoogleLocal = pkgs.haskellPackages.hoogleLocal.override {
              packages = hooglePackages;
            };
          };
        });
    };
  };

}
